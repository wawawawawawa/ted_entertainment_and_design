## 系统思维 

* 系统思维是一种逻辑抽象能力，也可以称为整体观、全局观。
另外，首先应该明白什么是系统?生物学中有[生态系统](http://baike.so.com/doc/2197078-2324693.html)，是指一个能够自我完善，达到动态平衡的生物链，如:一个池塘。 系统一般是可以封闭运作的，可以自我完善，并且能够动态平衡的物品集合
系统思维，简单来说就是对事情全面思考，不只就事论事。是把想要达到的结果、实现该结果的过程、过程优化以及对未来的影响等一系列问题作为一个整体系统进行研究。系统是一个概念，反映了人们对事物的一种认识论，即系统是由两个或两个以上的元素相结合的有机整体，系统的整体不等于其局部的简单相加。这一概念揭示了客观世界的某 种本质属性，有无限丰富的内涵和处延，其内容就是系统论或系统学。系统论作为一种普遍的方法论是迄今为止人类所掌握的最高级思维模式。系统思维是指以系统论为思维基本模式的思维形态，它不同于创造思维或[形象思维](http://baike.so.com/doc/6118405-6331550.html)等本能思维形态。系统思维能极大地简化人们对事物的认知，给我们带来整体观。按照历史时期来划分，可以把系统思维方式的演变区分为四个不同的发展阶段:古代整体系统思维方式--近代[机械系统](http://baike.so.com/doc/4114743-4313918.html)思维方式--辩证系统思维方式--现代复杂系统思维方式。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0616/193455_2249b3c5_2233429.jpeg "在这里输入图片标题")

## 权利

* "权利"一词在古代汉语里很早就有了，但大体上是消极的或贬义的，如，所谓"接之于声色、权利、愤怒、患险而观其能无离守也";"或尚仁义，或务权利"。这种语义上的权利不是一个可以用来构造法律关系的法学概念。[中国古代法律](http://baike.so.com/doc/6960000-7182511.)语言里也没有像英文"权利"、"义务"那样的词汇。19世纪中期，当美国学者丁韪良先生(W.A.P.Martin)和他的中国助手们把维顿(Wheaton)的《万国律例》(Elements of International Law)翻译成中文时，他们选择了"权利"这个古词来对译英文"rights"，并说服朝廷接受它。从此以后，"权利"在中国逐渐成了一个褒义的、至少是中性的词，并且被广泛使用。我们在此要考察的，就是后来的、或所谓现代意义上的"权利"一词的涵义。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0616/203356_6c9ae575_2233429.jpeg "在这里输入图片标题")

## 创新设计

* 创新设计是指充分发挥设计者的创造力，利用人类已有的相关科技成果进行创新构思，设计出具有科学性、创造性、新颖性及实用成果性的一种实践活动。创新是设计本质的要求，也是时代的要求。作为"为传达而设计"的[视觉传达设计](http://baike.so.com/doc/5390874-5627533.html)，如何正确充分地传达信息是每一个设计者始终要面临的中心问题。但是，在当今社会，仅仅把传达信息的关键词定位于正确和充分显然是不够的。必须要把视觉传达设计的创新重视起来，从设计理念、视觉语言和技术表现方式的创新入手，正确充分地传达信息。随着人类社会从工业化社会到信息化社会的发展，视觉传达设计经历了商业美术、工艺美术、印刷美术设计、装潢设计、平面设计等几大阶段的演变，最终成为以视觉媒介为载体，利用视觉符号表现并传达信息的设计。 "创新"一词对于我们来说并不陌生。江泽民同志曾经说过，"创新是一个民族进步的灵魂，是一个国家兴旺发达的不竭动力"。创新表现在人类社会历史发展的每一个方面。就设计界来说，创新同样也是设计的灵魂，是设计的本质要求。不论是纵观历史，还是着眼现实，一幅优秀的视觉传达设计作品，都是在创新的基础上，对其所表现设计主题的信息进行正确、充分地传达。在当代社会，大量低俗、模仿、毫无新意的设计充斥并污染着我们的视野。该是大力宣扬"设计创新"的时候了，因为这个时代比以往任何时期都更需要清晰而独创的视觉传达设计。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0616/205017_6a793693_2233429.jpeg "在这里输入图片标题")

## MORE

* [Leyla Acaroglu使用创新设计和系统思维来创造积极的变化](http://www.ted.com/speakers/leyla_acaroglu)