## 让电脑理解人类的情感

* 计算机非常擅于处理数据，但其智力方面的进展却是缓慢的。近些年来，计算机领域兴起了 “深度学习”。结合人工神经网络研究的成果，科学家们试图建立模拟人脑的神经网络，使计算机像人类一样思考，拥有学习能力。如今，“深度学习” 已经突破了学术圈的范畴，吸引了科技公司的资源投入，特别是以分析用户数据来盈利的公司，如 Facebook、Google 等。人工智能方面的突破，能使他们提供更加智能、人性化的服务。在 “深度学习” 研究中，自然语言的分析是重要部分。人类语言交流是非常复杂的，不仅涉及到意思的理解，也涉及到感情的表达，对于计算机来说，把握前者已经很困难，而把握后者恐怕更是难上加难。NaSent 项目试图在情感分析上面走出突破。这个项目由斯坦福大学的研究生 Richard Socher 发起，合作者包括人工智能研究员 Chris Manning，以及 Google 深度学习项目的工程师之一 Andrew Ng。“在过去，情感分析依赖的模式多是忽略单词次序的，或者依赖于人类专家，”Socher 接受 Wired 网站采访的时候说，“那适用于非常简单的例子，但是永远无法上升到人类水平的理解，因为单词的意思随语境而改变，即使是专家也无法准确定义情感运作的所有细节。我们的深度学习模式解决了这两个问题。”在构建 NaSent 的过程中，Socher 和他的团队从烂番茄网站拿来了 1.2 万个句子，将其拆分为 21.4 万个短语，标记为非常消极、消极、中性、积极、非常积极等，然后将这些数据输入系统之中，并以此为基础对句子做分析。
研究员们说，NaSent 的准确率是 85%。这个数字还是不错的。当然，算法还有很大的改善余地。为了构建更加强健的系统，Socher 的团队还向系统输入了 Twitter 和 IMDB 上的数据。另外，他们还建立了一个实时演示的网站，所有人都可以参与其中。如果 NaSent 的判断错误，人们可以对其判断进行重新标记。在几周的时间里，已经有 1.4 万个用户参与过这个演示。“人们很好心地教它新东西，告诉它正确与错误，”Socher 说，“给出实时演示的好处是，人们试图去破坏它。他们在把它推向极限，给予我们新的训练数据。这会帮助我们的算法模型。”![输入图片说明](https://images.gitee.com/uploads/images/2019/0518/134335_e6f19688_2233429.jpeg)

## 情绪是如何产生的，如何被大脑判断的

* 早期的理论认为情绪是自主神经系统的作用产生的，之后各种实验似乎找到了情绪变化在脑中的位置，所以首先，情绪是被大脑产生的，相关发现包括边缘系统中的杏仁核，下丘脑，大脑皮层的奖赏回路等，实际上恐惧，愤怒，愉快等情绪的产生都有了一些定位（fMRI的方法），当然这些还需要进一步研究就是了。关于判断，和认知有关，人的心理活动并非相互独立的，同样一件事，有人觉得很开心，有人觉得不愉快（比如甜咸豆腐脑之争），这就是最简单的例证。但是情绪的生理机制我们尚且没有完全了解，情绪和认知的交互就更是没有一个统一的结论了。最后，既然你单独提到了愤怒，首先婴儿早期可以分化出消极和积极情绪，然后是愤怒，惧怕这种简单情绪，最后是复杂情绪。![输入图片说明](https://images.gitee.com/uploads/images/2019/0518/134951_5bae275a_2233429.jpeg)

## 情绪管理的定义

* 情绪是个体对外界刺激的主观的有意识的体验和感受，具有心理和生理反应的特征。我们无法直接观测内在的感受，但是我们能够通过其外显的行为或生理变化来进行推断。意识状态是情绪体验的必要条件。情绪是身体对行为成功的可能性乃至必然性，在生理反应上的评价和体验，包括喜、怒、忧、思、悲、恐、惊七种。行为在身体动作上表现的越强就说明其情绪越强，如喜会是手舞足蹈、怒会是咬牙切齿、忧会是茶饭不思、悲会是痛心疾首等等就是情绪在身体动作上的反应。情绪是信心这一整体中的一部分，它与信心中的外向认知、外在意识具有协调一致性，是信心在生理上一种暂时的较剧烈的生理评价和体验美国哈佛大学心理学教授丹尼尔·戈尔曼认为:"情绪意指情感及其独特的思想、心理和生理状态，以及一系列行动的倾向。"
情绪不可能被完全消灭，但可以进行有效疏导、有效管理、适度控制。情绪无好坏之分，一般只划分为积极情绪、消极情绪。由情绪引发的行为则有好坏之分、行为的后果有好坏之分，所以说，情绪管理并非是消灭情绪，也没有必要消灭，而是疏导情绪、并合理化之后的信念与行为。这就是情绪管理的基本范畴。

## 如何处理自己的情绪

* 产生情绪的触发原因是客观事物本身，而不是主观需要，既有事就有情，有景就生情。心理学家把情绪的产生归之于三个因素的整合作用：刺激因素生理因素和认识因素。认识因素在情绪的表现形式中起着重要作用。这就是说有一定认识的人可以把握自己的情绪。一个人的工作顺逆、事业成败、人际关系好坏，都会引发不同的情绪。所以说：切忌因某一小事而怒发冲冠或垂头丧气。当一个人情绪好的时候，往往会精神振奋、干劲倍增，思维敏捷，效率提高；反之，则会无精打采，思路堵塞。效率下降。怎样才能保持健康稳定的情绪呢？笼统地讲要注意以下两点：用理智调节情绪。一个人能否保持清醒、冷静的头脑，用理智来控制支配自己的情绪，这是心理健康与否的重要标志。从心理学角度看，人们许多不愉快的情绪，多是自寻烦恼的结果。我想：人一定要正视现实，全面地多角度地看待问题，保持开阔的胸怀和乐观开朗的性格。要树立远大的志向。心理健康的人，有高尚的志向理想，有远大的奋斗目标。他们把“天下兴亡、匹夫有责”作为个人生命的整体认识和根本态度，因而不计较个人得失，名利大小，不会因为遇到一件小事，就悲观失望或高兴得发狂。他们能让自己的情绪按照正常的轨道前进、发展。有句格言：我是我心灵的主宰。掌握好自我情绪之舵，乘风破浪，在学习和工作中会做出不平凡的业绩，良好的心理素质能使你工作中心态平和、语言准确周密、简洁生动、脉络清晰、泾渭分明，使你的语言高雅、谦和、避免生硬、粗重，而且能使你语言受到“言之有理、言之有物、言之有情”的效果。![输入图片说明](https://images.gitee.com/uploads/images/2019/0518/140149_f62a1a2d_2233429.jpeg)

## More

* [•Why we should say no to positivity — and yes to our negative emotions](http://ideas.ted.com/why-we-should-say-no-to-positivity-and-yes-to-our-negative-emotions/)