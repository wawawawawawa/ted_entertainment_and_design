## 为什么说“编程改变世界”

* 知识就是力量”
培根说这句话的时候，英国刚刚处于干掉无敌舰队的志得意满中，似乎没有需要太多的知识来做到这些。此时距离牛爵士出生45年，第一台蒸汽机100年，第一台铁甲战舰下水两个世纪，第一台电脑三百五十年。任何一个大概能在脑海中画出指数函数形状的人，都可以尝试感受一下过去四百年的社会发展速度，套在指数函数模型上，从而切实的体会这知识种力量是何等令人窒息般的强大。

* 现在，在这个曲线的最前端，就是互联网对知识和思想的放大。所有的概念和交互结构以一种迷幻的，难以理解的有序相互勾连错落在这张网上，如同一张张多米诺骨牌，或者一颗颗黑白棋棋子一般。而一个懂得这张网的程序员，很有可能能找到那颗最关键的棋子，一翻之下，就能反转整个棋盘。这个棋子可以是yahoo，amazon，google，阿里巴巴，facebook，也可以是你，我，或者任何一个痴迷这个世界，尝试过翻了好几个以为有用却不曾撼动大盘的棋子的人。每当大家觉得格局确定了，总会有那么一两个程序员走出来，翻动那个真正能彻底改变棋盘的子。过去的很多传统行业都经历了从新兴到高速发展再到稳定继而沉闷的各个阶段。如同海上贸易，钢铁，石油，化工，机械，半导体等等。然而互联网和这些如此的不同，以至于经过了快三十年的发展，依然不断涌现出挑战你世界观的产品和概念。

## 编码如何改善更好的政府 

* 扁平化。没有哪个公司会有这么多的层级，国级、省级、市级、区级、镇级、村级，这么多的中间经销商，每个人都要自己的利益，当到达最终的消费者，也就是我们的时候，我们要付出的代价肯定是其高无比。借如果助于网络科技，比如银行网络，移动手机，就可以省掉很多中间的经销商，从省级总部甚至国级总部直接面对消费者，比如农村补贴的发放，儿童教育的补贴。 

* 拥抱互联网。没有哪一个企业在这个时代不去拥抱互联网。如果政府深度拥抱网络，那整个架构就从金字塔式变成了网络式。做事和思维的方式都不再是从上而下，而是从下而上，自愿参与。好在现在政府已经开始上路了，比如政务微博。 

* 社区化。邻居永远比政府更亲。如果现在的社区在社交联系上可以保持得如同以前的农村一样的良好，实际上对政府的需求是很少的，很多的问题都靠邻居解决了。更不用说现在是移动互联网时代，有啥事第一时间都可以了解到。如果能把城市真正的社区化，更大的社区，那从某种意义上说是一种“无政府”，不是说不需要政府这个组织，而是政府能给我们做的，邻居都帮我们做了，政府达到某种无为而治的状态了。这当然需要我们对公众事务承担更多的责任。 

## 黑客与骇客的意义

* 黑客代表是一种精神，它是一种热爱祖国、坚持正义、开拓进取的精神。黑客之道亦是侠客之道，所谓真正的黑客并非网上那些刷q.b、刷枪、刷车以及盗、QQ号、淘宝号、YY号，恶意攻击网站的这些人，他们只是无知无畏好奇心强而已，甚至驱使他们沦为一名恶作剧，搞破坏的一名骇客。黑客是指而是能够突破极限的能力，并拥有道德感、开放和共享的精神，最终的目的是推动世界发，展朝着一个更好、更安全的世界发展。第一，从数据到知识，一切都是信息。网络安全在中国是奢侈品，安全本质则是对信息的控制权。黑客是未来信息社会重要的平衡力量。

* 中国黑客协会致力于培养人才计划，对于有兴趣、热爱、执着的热血奋斗的青年，别让自己荒废着，新手永远是多数,我们将致力于新手启蒙和进阶教育,引导其树立正确观念，网络世界不安定因素过多。我们竭力让新人学会保护自己，树立正确的安全意识，提高其技术水准；我们有着严格的纪律，禁止任何成员利用本站名义或者以本站成员名义发表政治性言论，反动演说；我们任何成员或个人都不得以我们的名义进行攻击或破坏行为；我们锐意进取，成为网络安全世界的标兵，让中国的网络安全与全球同台竞技；我们不再由地域的阻隔，我们在网络下彼此相识，让互联网延伸彼此的友谊和文化；我们能共筑绿色的网络世界，在宁静和谐的网络中畅游；自由的人们总是不得不在自己的安全和自己的自由之间做出权衡。
真正的民族主义者以追求本民族――中华民族的利益最大化为目标、准则、信念。判断一个人是不是真正的民族主义者，判断标准很简单：看他是给本民族的整体利益带来好处，还是损害民族利益。真正的民族主义者最务实，因为他知道坚持原则，同时又懂得策略。黒协组织在遵守中华人民共和国有关法律的前提下开展和促进各类健康的网络文化的研究与交流。为我中华网络安全做贡献并愿意团结一切网络爱国团体和个人，为信息时代中华民族文化的伟大复兴而尽责！为推动中国的互联网发展而贡献!
 
## 代码才是未来世界的通用语言

* 从上世纪90年代计算机开始出现在人们的生活中，20多年的时间，从最早的Basic语言到互联网的普及，从互联网到移动互联网的快速崛起，计算机的发展历程可谓日新月异。牛津大学2013年发布的一份报告预测，未来20 年里有将近一半的工作可能被机器所取代。很多以现在的眼光来看很高大上，家长们正努力让孩子去学习以便将来从事的行业，也会有一部分被计算机淘汰。

* 当今社会正处在大数据时代，大数据、人工智能等技术将对未来社会产生重要的影响，所以学习IT技术是个不错的选择。近些年来，随着信息化技术的不断发展，各行各业与互联网的结合也越来越紧密，对于现代职场人来说，掌握一定的IT技术已经是一个基本的要求了。随着大数据相关技术的发展，未来职场对于IT技术的要求也会进一步提升，所以学习IT技术将具有更广泛的意义。